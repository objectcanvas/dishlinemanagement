﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fDesignations : Form
    {
        public fDesignations()
        {
            InitializeComponent();
        }
        private void fDesignations_Load(object sender, EventArgs e)
        {
            RefreshList();
        }


        private void RefreshList()
        {
            try
            {


                using (DishLineContext db = new DishLineContext())
                {
                    var _Designations = db.Designations;

                    ListViewItem item = null;
                    lsvDesignation.Items.Clear();

                    if (_Designations != null)
                    {
                        foreach (Designation grd in _Designations)
                        {
                            item = new ListViewItem();
                            item.Text = grd.Code;
                            item.SubItems.Add(grd.Name);
                            item.Tag = grd;
                            lsvDesignation.Items.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        #region Events
        private void btnNew_Click(object sender, EventArgs e)
        {
            fDesignation frm = new fDesignation();
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(new Designation());
           
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {

            if (lsvDesignation.SelectedItems.Count <= 0)
            {
                MessageBox.Show("select an item to edit", "Item not yet selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Designation Designation = null;
            fDesignation frm = new fDesignation();

            if (lsvDesignation.SelectedItems != null && lsvDesignation.SelectedItems.Count > 0)
            {
                Designation = (Designation)lsvDesignation.SelectedItems[0].Tag;
            }
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(Designation);
           
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Designation oDesignation = new Designation();
                if (lsvDesignation.SelectedItems != null && lsvDesignation.SelectedItems.Count > 0)
                {
                    oDesignation = (Designation)lsvDesignation.SelectedItems[0].Tag;
                    if (MessageBox.Show("Do you want to delete the selected item?", "Delete Setup", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        using (DishLineContext db = new DishLineContext())
                        {
                            db.Designations.Attach(oDesignation);
                            db.Designations.Remove(oDesignation);
                            //Save to database
                            db.SaveChanges();
                        }
                        RefreshList();
                    } 
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show("Cannot delete item due to " + Ex.Message);
            }
        }
        #endregion

       

      
       
    }
}
