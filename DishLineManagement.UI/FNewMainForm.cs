﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DishLineManagement.UI
{
    public partial class FNewMainForm : Form
    {
        string sUser = "";
        public FNewMainForm()
        {
            InitializeComponent();
        }

        private void FNewMainForm_Load(object sender, EventArgs e)
        {
            ToolStripStatusLabel4.Text = System.DateTime.Now.ToString("dd MMM yyyy HH:mm tt");
            sUser = lblUser.Text;

        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode.Tag != null)
            {
                switch (treeView1.SelectedNode.Tag.ToString())
                {
                    case "1.1":
                        //fSystemInformation oFSystemInfo = new fSystemInformation();
                        //oFSystemInfo.ShowDialog();
                        break;
                    case "1.2":
                        fDesignations ofDesignations = new fDesignations();
                        ofDesignations.ShowDialog();
                        break;
                    case "1.3":
                        fEmployees ofEmployees = new fEmployees();
                        ofEmployees.ShowDialog();
                        break;

                    case "1.4":
                        fOutlets ofOutlets = new fOutlets();
                        ofOutlets.ShowDialog();
                        break;
                    case "1.5":
                        fExpenditures ofExpenditures = new fExpenditures();
                        ofExpenditures.ShowDialog();
                        break;
                   
                    case "2.1":
                        fCustomers ofCustomers = new fCustomers();
                        ofCustomers.ShowDialog();
                        break;

                    case "3.1":
                        fInvoiceDetails oFInvoiceDetails = new fInvoiceDetails();
                        oFInvoiceDetails.ShowDialog();
                        break;

                    case "4.1":
                        //New User Registration
                        break;
                    case "4.2":
                        //User List
                        break;

                    case "5.1":
                        //Daily Sales Rpt
                        break;
                    case "5.2":
                        //Daily Purchase Rpt
                        break;
                    case "5.3":
                        //Weekly Sales Rpt
                        break;
                    case "5.4":
                        //Weekly Purchase Rpt
                        break;
                    case "5.5":
                        //Monthly Sales Rpt
                        break;
                    case "5.6":
                        //Monthly Purchase Rpt
                        break;
                    case "5.7":
                        //Yearly Sales Rpt
                        break;
                    case "5.8":
                        //Yearly Purchase Rpt
                        break;

                }

            }

        }
    }
}
