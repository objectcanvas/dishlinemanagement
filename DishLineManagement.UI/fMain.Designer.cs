﻿namespace DishLineManagement.UI
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCustomers = new System.Windows.Forms.Button();
            this.btnOutlets = new System.Windows.Forms.Button();
            this.btnEmployee = new System.Windows.Forms.Button();
            this.btnDesignation = new System.Windows.Forms.Button();
            this.btnExpense = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCustomers
            // 
            this.btnCustomers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomers.Location = new System.Drawing.Point(440, 25);
            this.btnCustomers.Name = "btnCustomers";
            this.btnCustomers.Size = new System.Drawing.Size(124, 72);
            this.btnCustomers.TabIndex = 1;
            this.btnCustomers.Text = "Customers";
            this.btnCustomers.UseVisualStyleBackColor = true;
            this.btnCustomers.Click += new System.EventHandler(this.btnCustomers_Click);
            // 
            // btnOutlets
            // 
            this.btnOutlets.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOutlets.Location = new System.Drawing.Point(297, 25);
            this.btnOutlets.Name = "btnOutlets";
            this.btnOutlets.Size = new System.Drawing.Size(124, 72);
            this.btnOutlets.TabIndex = 0;
            this.btnOutlets.Text = "Outlets";
            this.btnOutlets.UseVisualStyleBackColor = true;
            this.btnOutlets.Click += new System.EventHandler(this.btnOutlets_Click);
            // 
            // btnEmployee
            // 
            this.btnEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmployee.Location = new System.Drawing.Point(155, 25);
            this.btnEmployee.Name = "btnEmployee";
            this.btnEmployee.Size = new System.Drawing.Size(124, 72);
            this.btnEmployee.TabIndex = 2;
            this.btnEmployee.Text = "Employees";
            this.btnEmployee.UseVisualStyleBackColor = true;
            this.btnEmployee.Click += new System.EventHandler(this.btnEmployee_Click);
            // 
            // btnDesignation
            // 
            this.btnDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesignation.Location = new System.Drawing.Point(12, 25);
            this.btnDesignation.Name = "btnDesignation";
            this.btnDesignation.Size = new System.Drawing.Size(124, 72);
            this.btnDesignation.TabIndex = 3;
            this.btnDesignation.Text = "Designation";
            this.btnDesignation.UseVisualStyleBackColor = true;
            this.btnDesignation.Click += new System.EventHandler(this.btnDesignation_Click);
            // 
            // btnExpense
            // 
            this.btnExpense.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExpense.Location = new System.Drawing.Point(12, 121);
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.Size = new System.Drawing.Size(124, 72);
            this.btnExpense.TabIndex = 4;
            this.btnExpense.Text = "Expense";
            this.btnExpense.UseVisualStyleBackColor = true;
            this.btnExpense.Click += new System.EventHandler(this.btnExpense_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DishLineManagement.UI.Properties.Resources.Dekstop;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(659, 358);
            this.Controls.Add(this.btnExpense);
            this.Controls.Add(this.btnDesignation);
            this.Controls.Add(this.btnEmployee);
            this.Controls.Add(this.btnCustomers);
            this.Controls.Add(this.btnOutlets);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dish Line Management";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOutlets;
        private System.Windows.Forms.Button btnCustomers;
        private System.Windows.Forms.Button btnEmployee;
        private System.Windows.Forms.Button btnDesignation;
        private System.Windows.Forms.Button btnExpense;
    }
}

