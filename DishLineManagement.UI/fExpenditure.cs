﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fExpenditure : Form
    {
       
        private Expenditure _Expenditure = null;
        public Action ItemChanged;
        public fExpenditure()
        {
            InitializeComponent();
        }
        public void ShowDlg(Expenditure oExpenditure)
        {
            _Expenditure = oExpenditure;
           
            RefreshValue();
            this.ShowDialog();
            //return !_IsCanceled;
        }
       

        private void RefreshValue()
        {
            txtDescription.Text = _Expenditure.Description;
            dtpExpenseDate.Value = _Expenditure.ExpenseDate;
            numAmount.Value = _Expenditure.Amount;
        }

        private void RefreshObject()
        {
            _Expenditure.ExpenseDate = dtpExpenseDate.Value;
            _Expenditure.Description = txtDescription.Text;
            _Expenditure.Amount = numAmount.Value;
        }
        #region Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!InputValidate())
            //{
            //    return;
            //}
            if (MessageBox.Show("Do you want to save the information?", "Save Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {

                    using (DishLineContext db = new DishLineContext())
                    {
                        if (_Expenditure.ExpenditureID <= 0)
                        {
                            RefreshObject();
                            _Expenditure.ExpenditureID = db.Expenditures.Count()>0? db.Expenditures.Max(obj => obj.ExpenditureID) + 1:1;
                            db.Expenditures.Add(_Expenditure);
                        }
                        else
                        {
                            _Expenditure = db.Expenditures.FirstOrDefault(obj => obj.ExpenditureID == _Expenditure.ExpenditureID);
                            RefreshObject();
                        }

                        db.SaveChanges();
                        // _IsCanceled = false;
                        MessageBox.Show("Data saved successfully.", "Save Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //if (!_Expenditure.IsNew)
                        //{
                        //    this.Close();
                        //}
                        //else
                        //{
                        if (MessageBox.Show("Do you want to create another Expenditure?", "Create Expenditure", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            this.Close();
                        }
                        else
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            _Expenditure = new Expenditure();
                            RefreshValue();

                        }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                        MessageBox.Show(ex.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        MessageBox.Show(ex.InnerException.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
