﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DishLineManagement.UI
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnOutlets_Click(object sender, EventArgs e)
        {
            fOutlets frm = new fOutlets();
            frm.ShowDialog();

        }

        private void btnCustomers_Click(object sender, EventArgs e)
        {
            fCustomers frm = new fCustomers();
            frm.ShowDialog();
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            fEmployees frm = new fEmployees();
            frm.ShowDialog();
        }

        private void btnDesignation_Click(object sender, EventArgs e)
        {
            fDesignations frm = new fDesignations();
            frm.ShowDialog();
        }

        private void btnExpense_Click(object sender, EventArgs e)
        {
            fExpenditures frm = new fExpenditures();
            frm.ShowDialog();
        }
    }
}
