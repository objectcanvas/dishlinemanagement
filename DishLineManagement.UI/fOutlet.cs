﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fOutlet : Form
    {
        
        private Outlet _Outlet = null;
        public Action ItemChanged;
        public fOutlet()
        {
            InitializeComponent();
        }
        public void ShowDlg(Outlet oOutlet)
        {
            _Outlet = oOutlet;
            RefreshValue();
            this.ShowDialog();
            //return !_IsCanceled;
        }
        private void RefreshValue()
        {
            txtCode.Text = _Outlet.OutletCode;
            txtName.Text = _Outlet.OutletName;
        }

        private void RefreshObject()
        {

            _Outlet.OutletCode = txtCode.Text;
            _Outlet.OutletName = txtName.Text;
        }
        #region Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!InputValidate())
            //{
            //    return;
            //}
            if (MessageBox.Show("Do you want to save the information?", "Save Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {

                    using (DishLineContext db = new DishLineContext())
                    {
                        if (_Outlet.OutletID <= 0)
                        {
                            RefreshObject();
                            _Outlet.OutletID = db.Outlets.Count() > 0 ? db.Outlets.Max(obj => obj.OutletID) + 1 : 1;
                            db.Outlets.Add(_Outlet);
                        }
                        else
                        {
                            _Outlet = db.Outlets.FirstOrDefault(obj => obj.OutletID == _Outlet.OutletID);
                            RefreshObject();
                        }

                        db.SaveChanges();
                        // _IsCanceled = false;
                        MessageBox.Show("Data saved successfully.", "Save Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //if (!_Outlet.IsNew)
                        //{
                        //    this.Close();
                        //}
                        //else
                        //{
                        if (MessageBox.Show("Do you want to create another Outlet?", "Create Outlet", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            this.Close();
                        }
                        else
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            _Outlet = new Outlet();
                            RefreshValue();

                        }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                        MessageBox.Show(ex.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        MessageBox.Show(ex.InnerException.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        



        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
       

    }
}
