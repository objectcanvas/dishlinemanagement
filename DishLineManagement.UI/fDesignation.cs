﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fDesignation : Form
    {
        
        private Designation _Designation = null;
        public Action ItemChanged;
        public fDesignation()
        {
            InitializeComponent();
        }
        public void ShowDlg(Designation oDesignation)
        {
            _Designation = oDesignation;
            RefreshValue();
            this.ShowDialog();
            //return !_IsCanceled;
        }
        private void RefreshValue()
        {
            txtCode.Text = _Designation.Code;
            txtName.Text = _Designation.Name;
        }

        private void RefreshObject()
        {

            _Designation.Code = txtCode.Text;
            _Designation.Name = txtName.Text;
        }
        #region Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!InputValidate())
            //{
            //    return;
            //}
            if (MessageBox.Show("Do you want to save the information?", "Save Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {

                    using (DishLineContext db = new DishLineContext())
                    {
                        if (_Designation.DesignationID <= 0)
                        {
                            RefreshObject();
                            _Designation.DesignationID = db.Designations.Count() > 0 ? db.Designations.Max(obj => obj.DesignationID) + 1 : 1;
                            db.Designations.Add(_Designation);
                        }
                        else
                        {
                            _Designation = db.Designations.FirstOrDefault(obj => obj.DesignationID == _Designation.DesignationID);
                            RefreshObject();
                        }

                        db.SaveChanges();
                        // _IsCanceled = false;
                        MessageBox.Show("Data saved successfully.", "Save Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //if (!_Designation.IsNew)
                        //{
                        //    this.Close();
                        //}
                        //else
                        //{
                        if (MessageBox.Show("Do you want to create another Designation?", "Create Designation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            this.Close();
                        }
                        else
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            _Designation = new Designation();
                            RefreshValue();

                        }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                        MessageBox.Show(ex.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        MessageBox.Show(ex.InnerException.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        



        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
       

    }
}
