﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fEmployees : Form
    {
        public fEmployees()
        {
            InitializeComponent();
        }
        private void fEmployees_Load(object sender, EventArgs e)
        {
            RefreshList();
        }


        private void RefreshList()
        {
            try
            {


                using (DishLineContext db = new DishLineContext())
                {
                    var _Employees = db.Employees;

                    ListViewItem item = null;
                    lsvEmployee.Items.Clear();

                    if (_Employees != null)
                    {
                        foreach (Employee grd in _Employees)
                        {
                            item = new ListViewItem();
                            item.Text = grd.EmpCode;
                            item.SubItems.Add(grd.EmpName);
                            item.Tag = grd;
                            lsvEmployee.Items.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        #region Events
        private void btnNew_Click(object sender, EventArgs e)
        {
            fEmployee frm = new fEmployee();
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(new Employee());
           
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {

            if (lsvEmployee.SelectedItems.Count <= 0)
            {
                MessageBox.Show("select an item to edit", "Item not yet selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Employee Employee = null;
            fEmployee frm = new fEmployee();

            if (lsvEmployee.SelectedItems != null && lsvEmployee.SelectedItems.Count > 0)
            {
                Employee = (Employee)lsvEmployee.SelectedItems[0].Tag;
            }
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(Employee);
           
        }
        #endregion

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Employee oEmployee = new Employee();
                if (lsvEmployee.SelectedItems != null && lsvEmployee.SelectedItems.Count > 0)
                {
                    oEmployee = (Employee)lsvEmployee.SelectedItems[0].Tag;
                    if (MessageBox.Show("Do you want to delete the selected item?", "Delete Setup", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        using (DishLineContext db = new DishLineContext())
                        {
                            db.Employees.Remove(oEmployee);
                            //Save to database
                            db.SaveChanges();
                        }
                        RefreshList();
                    }
                }
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Cannot delete item due to " + Ex.Message);
            }
        }

      
       
    }
}
