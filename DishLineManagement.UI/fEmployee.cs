﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fEmployee : Form
    {
       
        private Employee _Employee = null;
        public Action ItemChanged;
        public fEmployee()
        {
            InitializeComponent();
        }
        public void ShowDlg(Employee oEmployee)
        {
            _Employee = oEmployee;
            PopulateControl();
            RefreshValue();
            this.ShowDialog();
            //return !_IsCanceled;
        }
        private void PopulateControl()
        {
            using(DishLineContext db=new DishLineContext())
            {
                var oOutlets = db.Outlets;
           
           cboDesignation.DataSource=db.Designations.ToList();
           cboDesignation.DisplayMember = "Name";
           cboDesignation.ValueMember = "DesignationID";
            //    cboOutlet.Items.Clear();
            //foreach (Outlet Item in oOutlets)
            //{
            //    cboOutlet..Items.Add(Item);
            //}

            //cboOutlet.DisplayMember = "Name";
            //cboOutlet.ValueMember = "OutletID";
            //if (cboOutlet.Items.Count > 0)
            //    cboOutlet.SelectedIndex = 0;
            }
        }

        private void RefreshValue()
        {
            txtCode.Text = _Employee.EmpCode;
            txtName.Text = _Employee.EmpName;
           txtFatherName.Text= _Employee.FatherName ;
            txtMotherName.Text=_Employee.MotherName;
            txtContactNo.Text= _Employee.ContactNo;
            txtPresentAdd.Text=_Employee.PresentAdd;
            txtpermanentAdd.Text=_Employee.PermanentAdd;
            if (_Employee.PhotoPath != null && _Employee.PhotoPath != "")
            {
                txtPhotoPath.Text = _Employee.PhotoPath;
                pictureBox1.Image = new Bitmap(_Employee.PhotoPath);
            }
                cboDesignation.SelectedValue = _Employee.DesignationID;
        }

        private void RefreshObject()
        {

            _Employee.EmpCode = txtCode.Text;
            _Employee.EmpName = txtName.Text;
            _Employee.FatherName = txtFatherName.Text;
            _Employee.MotherName = txtMotherName.Text;
            _Employee.ContactNo = txtContactNo.Text;
            _Employee.PresentAdd = txtPresentAdd.Text;
            _Employee.PermanentAdd = txtpermanentAdd.Text;
            _Employee.DesignationID = ((Designation)cboDesignation.SelectedItem).DesignationID;
            _Employee.PhotoPath = txtPhotoPath.Text;

        }
        #region Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!InputValidate())
            //{
            //    return;
            //}
            if (MessageBox.Show("Do you want to save the information?", "Save Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {

                    using (DishLineContext db = new DishLineContext())
                    {
                        if (_Employee.EmployeeID <= 0)
                        {
                            RefreshObject();
                            _Employee.EmployeeID = db.Employees.Count()>0? db.Employees.Max(obj => obj.EmployeeID) + 1:1;
                            db.Employees.Add(_Employee);
                        }
                        else
                        {
                            _Employee = db.Employees.FirstOrDefault(obj => obj.EmployeeID == _Employee.EmployeeID);
                            RefreshObject();
                        }

                        db.SaveChanges();
                        // _IsCanceled = false;
                        MessageBox.Show("Data saved successfully.", "Save Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //if (!_Employee.IsNew)
                        //{
                        //    this.Close();
                        //}
                        //else
                        //{
                        if (MessageBox.Show("Do you want to create another Employee?", "Create Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            this.Close();
                        }
                        else
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            _Employee = new Employee();
                            RefreshValue();

                        }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                        MessageBox.Show(ex.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        MessageBox.Show(ex.InnerException.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
        #endregion

        private void btnImage_Click(object sender, EventArgs e)
        {
             // open file dialog 
            OpenFileDialog open = new OpenFileDialog();
            // image filters
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box
                pictureBox1.Image = new Bitmap(open.FileName);
                // image file path
                txtPhotoPath.Text = open.FileName;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            pictureBox1.Image=null;
           // pictureBox1.Image.Dispose();
            txtPhotoPath.Text = "";
        }
       

    }
}
