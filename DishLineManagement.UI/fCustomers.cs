﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fCustomers : Form
    {
        public fCustomers()
        {
            InitializeComponent();
        }
        private void fCustomers_Load(object sender, EventArgs e)
        {
            RefreshList();
        }


        private void RefreshList()
        {
            try
            {


                using (DishLineContext db = new DishLineContext())
                {
                    var _Customers = db.Customers;

                    ListViewItem item = null;
                    lsvCustomer.Items.Clear();

                    if (_Customers != null)
                    {
                        foreach (Customer grd in _Customers)
                        {
                            item = new ListViewItem();
                            item.Text = grd.Code;
                            item.SubItems.Add(grd.Name);
                            item.Tag = grd;
                            lsvCustomer.Items.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        #region Events
        private void btnNew_Click(object sender, EventArgs e)
        {
            fCustomer frm = new fCustomer();
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(new Customer());
           
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {

            if (lsvCustomer.SelectedItems.Count <= 0)
            {
                MessageBox.Show("select an item to edit", "Item not yet selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Customer Customer = null;
            fCustomer frm = new fCustomer();

            if (lsvCustomer.SelectedItems != null && lsvCustomer.SelectedItems.Count > 0)
            {
                Customer = (Customer)lsvCustomer.SelectedItems[0].Tag;
            }
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(Customer);
           
        }
        #endregion

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Customer oCustomer = new Customer();
                if (lsvCustomer.SelectedItems != null && lsvCustomer.SelectedItems.Count > 0)
                {
                    oCustomer = (Customer)lsvCustomer.SelectedItems[0].Tag;
                    if (MessageBox.Show("Do you want to delete the selected item?", "Delete Setup", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        using (DishLineContext db = new DishLineContext())
                        {
                            db.Customers.Remove(oCustomer);
                            //Save to database
                            db.SaveChanges();
                        }
                        RefreshList();
                    }
                }
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Cannot delete item due to " + Ex.Message);
            }
        }

      
       
    }
}
