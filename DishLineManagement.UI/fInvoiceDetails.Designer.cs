﻿namespace DishLineManagement.UI
{
    partial class fInvoiceDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcInvoice = new System.Windows.Forms.TabControl();
            this.tbpAllInvoice = new System.Windows.Forms.TabPage();
            this.lsvAllInvoice = new System.Windows.Forms.ListView();
            this.clnInvoiceNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnOutlet = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnCustomer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnRecDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnRecAmt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnPaymentDue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnTotalAmt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbpNewInvoice = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lsv = new System.Windows.Forms.ListView();
            this.clnInvoice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnInvOutlet = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnInvCus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnMonth = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clnAmt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpPaymentMonth = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpRecvDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.cboOutlet = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbpIndInvoice = new System.Windows.Forms.TabPage();
            this.lsvIndInvoice = new System.Windows.Forms.ListView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnIndivClose = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.cboInvCustomer = new System.Windows.Forms.ComboBox();
            this.cboInvOutlet = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tcInvoice.SuspendLayout();
            this.tbpAllInvoice.SuspendLayout();
            this.tbpNewInvoice.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbpIndInvoice.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcInvoice
            // 
            this.tcInvoice.Controls.Add(this.tbpAllInvoice);
            this.tcInvoice.Controls.Add(this.tbpNewInvoice);
            this.tcInvoice.Controls.Add(this.tbpIndInvoice);
            this.tcInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcInvoice.Location = new System.Drawing.Point(9, 7);
            this.tcInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.tcInvoice.Name = "tcInvoice";
            this.tcInvoice.SelectedIndex = 0;
            this.tcInvoice.Size = new System.Drawing.Size(950, 580);
            this.tcInvoice.TabIndex = 0;
            // 
            // tbpAllInvoice
            // 
            this.tbpAllInvoice.BackColor = System.Drawing.SystemColors.Control;
            this.tbpAllInvoice.Controls.Add(this.lsvAllInvoice);
            this.tbpAllInvoice.Location = new System.Drawing.Point(4, 25);
            this.tbpAllInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.tbpAllInvoice.Name = "tbpAllInvoice";
            this.tbpAllInvoice.Padding = new System.Windows.Forms.Padding(4);
            this.tbpAllInvoice.Size = new System.Drawing.Size(942, 551);
            this.tbpAllInvoice.TabIndex = 0;
            this.tbpAllInvoice.Text = "All Invoice";
            // 
            // lsvAllInvoice
            // 
            this.lsvAllInvoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnInvoiceNo,
            this.clnOutlet,
            this.clnCustomer,
            this.clnRecDate,
            this.clnRecAmt,
            this.clnPaymentDue,
            this.clnTotalAmt});
            this.lsvAllInvoice.GridLines = true;
            this.lsvAllInvoice.Location = new System.Drawing.Point(8, 7);
            this.lsvAllInvoice.Name = "lsvAllInvoice";
            this.lsvAllInvoice.Size = new System.Drawing.Size(923, 534);
            this.lsvAllInvoice.TabIndex = 0;
            this.lsvAllInvoice.UseCompatibleStateImageBehavior = false;
            this.lsvAllInvoice.View = System.Windows.Forms.View.Details;
            // 
            // clnInvoiceNo
            // 
            this.clnInvoiceNo.Text = "Invoice No";
            this.clnInvoiceNo.Width = 104;
            // 
            // clnOutlet
            // 
            this.clnOutlet.Text = "Outlet";
            this.clnOutlet.Width = 169;
            // 
            // clnCustomer
            // 
            this.clnCustomer.Text = "Customer";
            this.clnCustomer.Width = 206;
            // 
            // clnRecDate
            // 
            this.clnRecDate.Text = "Receive Date";
            this.clnRecDate.Width = 99;
            // 
            // clnRecAmt
            // 
            this.clnRecAmt.Text = "Receive Amount";
            this.clnRecAmt.Width = 115;
            // 
            // clnPaymentDue
            // 
            this.clnPaymentDue.Text = "Payment Due";
            this.clnPaymentDue.Width = 102;
            // 
            // clnTotalAmt
            // 
            this.clnTotalAmt.Text = "Total Amount";
            this.clnTotalAmt.Width = 103;
            // 
            // tbpNewInvoice
            // 
            this.tbpNewInvoice.BackColor = System.Drawing.SystemColors.Control;
            this.tbpNewInvoice.Controls.Add(this.panel2);
            this.tbpNewInvoice.Location = new System.Drawing.Point(4, 25);
            this.tbpNewInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.tbpNewInvoice.Name = "tbpNewInvoice";
            this.tbpNewInvoice.Padding = new System.Windows.Forms.Padding(4);
            this.tbpNewInvoice.Size = new System.Drawing.Size(942, 551);
            this.tbpNewInvoice.TabIndex = 1;
            this.tbpNewInvoice.Text = "New Invoice";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(8, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(925, 531);
            this.panel2.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.lsv);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.dtpPaymentMonth);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(8, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(908, 420);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Payment Details";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnSave);
            this.groupBox3.Location = new System.Drawing.Point(10, 358);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(888, 55);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(786, 17);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(94, 31);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(687, 18);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 31);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(109, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(167, 23);
            this.textBox2.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Monthly Fee";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(453, 48);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(146, 23);
            this.textBox6.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(360, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 17);
            this.label10.TabIndex = 15;
            this.label10.Text = "Current Due";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(749, 18);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(147, 23);
            this.textBox3.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnRemove);
            this.panel4.Controls.Add(this.btnAdd);
            this.panel4.Location = new System.Drawing.Point(802, 92);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(96, 263);
            this.panel4.TabIndex = 14;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(8, 43);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 26);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(8, 11);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 26);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lsv
            // 
            this.lsv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clnInvoice,
            this.clnInvOutlet,
            this.clnInvCus,
            this.clnMonth,
            this.clnAmt});
            this.lsv.GridLines = true;
            this.lsv.Location = new System.Drawing.Point(10, 91);
            this.lsv.Name = "lsv";
            this.lsv.Size = new System.Drawing.Size(786, 265);
            this.lsv.TabIndex = 13;
            this.lsv.UseCompatibleStateImageBehavior = false;
            this.lsv.View = System.Windows.Forms.View.Details;
            // 
            // clnInvoice
            // 
            this.clnInvoice.Text = "Invoice No";
            this.clnInvoice.Width = 110;
            // 
            // clnInvOutlet
            // 
            this.clnInvOutlet.Text = "Oulet";
            this.clnInvOutlet.Width = 180;
            // 
            // clnInvCus
            // 
            this.clnInvCus.Text = "Customer";
            this.clnInvCus.Width = 214;
            // 
            // clnMonth
            // 
            this.clnMonth.Text = "Month";
            this.clnMonth.Width = 127;
            // 
            // clnAmt
            // 
            this.clnAmt.Text = "Amount";
            this.clnAmt.Width = 137;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(453, 21);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(146, 23);
            this.textBox4.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(644, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Previous Due";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(360, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "Receive Amt";
            // 
            // dtpPaymentMonth
            // 
            this.dtpPaymentMonth.CustomFormat = "MMM yyyy";
            this.dtpPaymentMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPaymentMonth.Location = new System.Drawing.Point(109, 23);
            this.dtpPaymentMonth.Name = "dtpPaymentMonth";
            this.dtpPaymentMonth.Size = new System.Drawing.Size(167, 23);
            this.dtpPaymentMonth.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Pay Month";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(748, 51);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(147, 23);
            this.textBox5.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(643, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 11;
            this.label9.Text = "Total Amount";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpRecvDate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboCustomer);
            this.groupBox1.Controls.Add(this.cboOutlet);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(909, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customer Details";
            // 
            // dtpRecvDate
            // 
            this.dtpRecvDate.CustomFormat = "dd MMM yyyy";
            this.dtpRecvDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRecvDate.Location = new System.Drawing.Point(110, 52);
            this.dtpRecvDate.Name = "dtpRecvDate";
            this.dtpRecvDate.Size = new System.Drawing.Size(167, 23);
            this.dtpRecvDate.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Receive Date";
            // 
            // cboCustomer
            // 
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.Location = new System.Drawing.Point(663, 49);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(224, 24);
            this.cboCustomer.TabIndex = 5;
            // 
            // cboOutlet
            // 
            this.cboOutlet.FormattingEnabled = true;
            this.cboOutlet.Location = new System.Drawing.Point(663, 18);
            this.cboOutlet.Name = "cboOutlet";
            this.cboOutlet.Size = new System.Drawing.Size(224, 24);
            this.cboOutlet.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(110, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(167, 23);
            this.textBox1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(566, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Customer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(570, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Outlet";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Invoice No";
            // 
            // tbpIndInvoice
            // 
            this.tbpIndInvoice.BackColor = System.Drawing.SystemColors.Control;
            this.tbpIndInvoice.Controls.Add(this.lsvIndInvoice);
            this.tbpIndInvoice.Controls.Add(this.panel5);
            this.tbpIndInvoice.Controls.Add(this.panel1);
            this.tbpIndInvoice.Location = new System.Drawing.Point(4, 25);
            this.tbpIndInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.tbpIndInvoice.Name = "tbpIndInvoice";
            this.tbpIndInvoice.Size = new System.Drawing.Size(942, 551);
            this.tbpIndInvoice.TabIndex = 2;
            this.tbpIndInvoice.Text = "Show Individual Invoice";
            // 
            // lsvIndInvoice
            // 
            this.lsvIndInvoice.GridLines = true;
            this.lsvIndInvoice.Location = new System.Drawing.Point(10, 124);
            this.lsvIndInvoice.Name = "lsvIndInvoice";
            this.lsvIndInvoice.Size = new System.Drawing.Size(922, 361);
            this.lsvIndInvoice.TabIndex = 2;
            this.lsvIndInvoice.UseCompatibleStateImageBehavior = false;
            this.lsvIndInvoice.View = System.Windows.Forms.View.Details;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnIndivClose);
            this.panel5.Controls.Add(this.btnPreview);
            this.panel5.Location = new System.Drawing.Point(10, 493);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(922, 47);
            this.panel5.TabIndex = 1;
            // 
            // btnIndivClose
            // 
            this.btnIndivClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnIndivClose.Location = new System.Drawing.Point(814, 7);
            this.btnIndivClose.Name = "btnIndivClose";
            this.btnIndivClose.Size = new System.Drawing.Size(102, 31);
            this.btnIndivClose.TabIndex = 1;
            this.btnIndivClose.Text = "&Close";
            this.btnIndivClose.UseVisualStyleBackColor = true;
            this.btnIndivClose.Click += new System.EventHandler(this.btnIndivClose_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(705, 7);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(102, 31);
            this.btnPreview.TabIndex = 0;
            this.btnPreview.Text = "&Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.dtpToDate);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.dtpFromDate);
            this.panel1.Controls.Add(this.cboInvCustomer);
            this.panel1.Controls.Add(this.cboInvOutlet);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Location = new System.Drawing.Point(10, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(921, 107);
            this.panel1.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(770, 70);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(118, 30);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Text = "&Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(561, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "To Month";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "MMM yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(656, 41);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(234, 23);
            this.dtpToDate.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 17);
            this.label13.TabIndex = 11;
            this.label13.Text = "From Month";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(106, 43);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(234, 23);
            this.dtpFromDate.TabIndex = 10;
            // 
            // cboInvCustomer
            // 
            this.cboInvCustomer.FormattingEnabled = true;
            this.cboInvCustomer.Location = new System.Drawing.Point(654, 10);
            this.cboInvCustomer.Name = "cboInvCustomer";
            this.cboInvCustomer.Size = new System.Drawing.Size(235, 24);
            this.cboInvCustomer.TabIndex = 9;
            // 
            // cboInvOutlet
            // 
            this.cboInvOutlet.FormattingEnabled = true;
            this.cboInvOutlet.Location = new System.Drawing.Point(106, 13);
            this.cboInvOutlet.Name = "cboInvOutlet";
            this.cboInvOutlet.Size = new System.Drawing.Size(235, 24);
            this.cboInvOutlet.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(558, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "Customer";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Outlet";
            // 
            // fInvoiceDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnIndivClose;
            this.ClientSize = new System.Drawing.Size(963, 593);
            this.Controls.Add(this.tcInvoice);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "fInvoiceDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Invoice Details";
            this.Load += new System.EventHandler(this.fInvoiceDetails_Load);
            this.tcInvoice.ResumeLayout(false);
            this.tbpAllInvoice.ResumeLayout(false);
            this.tbpNewInvoice.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbpIndInvoice.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcInvoice;
        private System.Windows.Forms.TabPage tbpAllInvoice;
        private System.Windows.Forms.TabPage tbpNewInvoice;
        private System.Windows.Forms.TabPage tbpIndInvoice;
        private System.Windows.Forms.ListView lsvAllInvoice;
        private System.Windows.Forms.ColumnHeader clnInvoiceNo;
        private System.Windows.Forms.ColumnHeader clnOutlet;
        private System.Windows.Forms.ColumnHeader clnCustomer;
        private System.Windows.Forms.ColumnHeader clnRecDate;
        private System.Windows.Forms.ColumnHeader clnRecAmt;
        private System.Windows.Forms.ColumnHeader clnPaymentDue;
        private System.Windows.Forms.ColumnHeader clnTotalAmt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpRecvDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.ComboBox cboOutlet;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpPaymentMonth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListView lsv;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cboInvCustomer;
        private System.Windows.Forms.ComboBox cboInvOutlet;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListView lsvIndInvoice;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnIndivClose;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ColumnHeader clnInvoice;
        private System.Windows.Forms.ColumnHeader clnInvOutlet;
        private System.Windows.Forms.ColumnHeader clnInvCus;
        private System.Windows.Forms.ColumnHeader clnMonth;
        private System.Windows.Forms.ColumnHeader clnAmt;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
    }
}