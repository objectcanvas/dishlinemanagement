﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fExpenditures : Form
    {
        public fExpenditures()
        {
            InitializeComponent();
        }
        private void fExpenditures_Load(object sender, EventArgs e)
        {
            RefreshList();
        }


        private void RefreshList()
        {
            try
            {


                using (DishLineContext db = new DishLineContext())
                {
                    var _Expenditures = db.Expenditures;

                    ListViewItem item = null;
                    lsvExpenditure.Items.Clear();

                    if (_Expenditures != null)
                    {
                        foreach (Expenditure grd in _Expenditures)
                        {
                            item = new ListViewItem();
                            item.Text = grd.Description;
                            item.SubItems.Add(grd.ExpenseDate.ToString("dd MMM yyyy"));
                            item.SubItems.Add(grd.Amount.ToString());
                            item.Tag = grd;
                            lsvExpenditure.Items.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        #region Events
        private void btnNew_Click(object sender, EventArgs e)
        {
            fExpenditure frm = new fExpenditure();
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(new Expenditure());
           
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {

            if (lsvExpenditure.SelectedItems.Count <= 0)
            {
                MessageBox.Show("select an item to edit", "Item not yet selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Expenditure Expenditure = null;
            fExpenditure frm = new fExpenditure();

            if (lsvExpenditure.SelectedItems != null && lsvExpenditure.SelectedItems.Count > 0)
            {
                Expenditure = (Expenditure)lsvExpenditure.SelectedItems[0].Tag;
            }
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(Expenditure);
           
        }
        #endregion

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Expenditure oExpenditure = new Expenditure();
                if (lsvExpenditure.SelectedItems != null && lsvExpenditure.SelectedItems.Count > 0)
                {
                    oExpenditure = (Expenditure)lsvExpenditure.SelectedItems[0].Tag;
                    if (MessageBox.Show("Do you want to delete the selected item?", "Delete Setup", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        using (DishLineContext db = new DishLineContext())
                        {
                            db.Expenditures.Remove(oExpenditure);
                            //Save to database
                            db.SaveChanges();
                        }
                        RefreshList();
                    }
                }
                
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Cannot delete item due to " + Ex.Message);
            }
        }

      
       
    }
}
