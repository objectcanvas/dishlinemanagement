﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fCustomer : Form
    {
       
        private Customer _Customer = null;
        public Action ItemChanged;
        public fCustomer()
        {
            InitializeComponent();
        }
        public void ShowDlg(Customer oCustomer)
        {
            _Customer = oCustomer;
            PopulateControl();
            RefreshValue();
            this.ShowDialog();
            //return !_IsCanceled;
        }
        private void PopulateControl()
        {
            using(DishLineContext db=new DishLineContext())
            {
                var oOutlets = db.Outlets;
           
           cboOutlet.DataSource=db.Outlets.ToList();
                 cboOutlet.DisplayMember = "OutletName";
            cboOutlet.ValueMember = "OutletID";
            //    cboOutlet.Items.Clear();
            //foreach (Outlet Item in oOutlets)
            //{
            //    cboOutlet..Items.Add(Item);
            //}

            //cboOutlet.DisplayMember = "Name";
            //cboOutlet.ValueMember = "OutletID";
            //if (cboOutlet.Items.Count > 0)
            //    cboOutlet.SelectedIndex = 0;
            }
        }

        private void RefreshValue()
        {
            txtCode.Text = _Customer.Code;
            txtName.Text = _Customer.Name;
           txtFatherName.Text= _Customer.FatherName ;
            txtMotherName.Text=_Customer.MotherName;
            txtContactNo.Text= _Customer.ContactNo;
            dtpEntryDate.Value=_Customer.EntryDate;
            numEntryFee.Value=_Customer.EntryFee ;
            numMonthlyCharge.Value=_Customer.MonthlyCharge ;
           cboOutlet.SelectedValue= _Customer.OutletID;
        }

        private void RefreshObject()
        {

            _Customer.Code = txtCode.Text;
            _Customer.Name = txtName.Text;
            _Customer.FatherName = txtFatherName.Text;
            _Customer.MotherName = txtMotherName.Text;
            _Customer.ContactNo = txtContactNo.Text;
            _Customer.EntryDate = dtpEntryDate.Value;
            _Customer.EntryFee = numEntryFee.Value;
            _Customer.MonthlyCharge = numMonthlyCharge.Value;
            _Customer.OutletID = ((Outlet)cboOutlet.SelectedItem).OutletID;

        }
        #region Events
        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (!InputValidate())
            //{
            //    return;
            //}
            if (MessageBox.Show("Do you want to save the information?", "Save Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {

                    using (DishLineContext db = new DishLineContext())
                    {
                        if (_Customer.CustomerID <= 0)
                        {
                            RefreshObject();
                            _Customer.CustomerID = db.Customers.Count()>0? db.Customers.Max(obj => obj.CustomerID) + 1:1;
                            db.Customers.Add(_Customer);
                        }
                        else
                        {
                            _Customer = db.Customers.FirstOrDefault(obj => obj.CustomerID == _Customer.CustomerID);
                            RefreshObject();
                        }

                        db.SaveChanges();
                        // _IsCanceled = false;
                        MessageBox.Show("Data saved successfully.", "Save Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //if (!_Customer.IsNew)
                        //{
                        //    this.Close();
                        //}
                        //else
                        //{
                        if (MessageBox.Show("Do you want to create another Customer?", "Create Customer", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            this.Close();
                        }
                        else
                        {
                            if (ItemChanged != null)
                            {
                                ItemChanged();
                            }
                            _Customer = new Customer();
                            RefreshValue();

                        }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                        MessageBox.Show(ex.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        MessageBox.Show(ex.InnerException.Message, "Failed to save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
       

    }
}
