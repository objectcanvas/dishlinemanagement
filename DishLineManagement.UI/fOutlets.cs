﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DishLineManagement.DA;

namespace DishLineManagement.UI
{
    public partial class fOutlets : Form
    {
        public fOutlets()
        {
            InitializeComponent();
        }
        private void fOutlets_Load(object sender, EventArgs e)
        {
            RefreshList();
        }


        private void RefreshList()
        {
            try
            {


                using (DishLineContext db = new DishLineContext())
                {
                    var _Outlets = db.Outlets;

                    ListViewItem item = null;
                    lsvOutlet.Items.Clear();

                    if (_Outlets != null)
                    {
                        foreach (Outlet grd in _Outlets)
                        {
                            item = new ListViewItem();
                            item.Text = grd.OutletCode;
                            item.SubItems.Add(grd.OutletName);
                            item.Tag = grd;
                            lsvOutlet.Items.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        #region Events
        private void btnNew_Click(object sender, EventArgs e)
        {
            fOutlet frm = new fOutlet();
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(new Outlet());
           
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {

            if (lsvOutlet.SelectedItems.Count <= 0)
            {
                MessageBox.Show("select an item to edit", "Item not yet selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Outlet outlet = null;
            fOutlet frm = new fOutlet();

            if (lsvOutlet.SelectedItems != null && lsvOutlet.SelectedItems.Count > 0)
            {
                outlet = (Outlet)lsvOutlet.SelectedItems[0].Tag;
            }
            frm.ItemChanged = RefreshList;
            frm.ShowDlg(outlet);
           
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Outlet oOutlet = new Outlet();
                if (lsvOutlet.SelectedItems != null && lsvOutlet.SelectedItems.Count > 0)
                {
                    oOutlet = (Outlet)lsvOutlet.SelectedItems[0].Tag;
                    if (MessageBox.Show("Do you want to delete the selected item?", "Delete Setup", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        using (DishLineContext db = new DishLineContext())
                        {
                            db.Outlets.Attach(oOutlet);
                            db.Outlets.Remove(oOutlet);
                            //Save to database
                            db.SaveChanges();
                        }
                        RefreshList();
                    } 
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show("Cannot delete item due to " + Ex.Message);
            }
        }
        #endregion

       

      
       
    }
}
